/*
 * Copyright (C) 2020-2021 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.asclepios.tomcat;

import eu.asclepios.authorization.abac.util.CheckAccessResponse;

import java.io.IOException;
import java.util.logging.Logger;
import java.util.logging.Level;

import org.apache.catalina.connector.Request;
import org.apache.catalina.connector.Response;
import org.apache.catalina.valves.ValveBase;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ABACValve extends ValveBase {
    private static final Logger logger = Logger.getLogger(ABACValve.class.getName());

    public void invoke(Request request, Response response) throws IOException, ServletException {
        HttpServletRequest req = request.getRequest();
        HttpServletResponse res = response.getResponse();

        if (checkAccess(req, res)) {
            getNext().invoke(request, response);
        }
    }

    private boolean checkAccess(HttpServletRequest servletRequest, HttpServletResponse servletResponse) throws IOException {
        CheckAccessResponse response;
        boolean permit;
        try {
            logger.log(Level.INFO, "AZ:    Calling PDP...");
            response = eu.asclepios.authorization.abac.client.WebAccessRequestHelper.getSingleton()
                    .getCheckWebAccessResponse(servletRequest, "resource", "action", "subject", null);
            permit = (response == null) || response.isPermit();
            logger.log(Level.FINE, "AZ:    PDP response: {}", response);
            logger.log(Level.WARNING, "AZ:    PDP decision: {}", permit?"PERMIT":"DENY");
        } catch (Exception e) {
            logger.log(Level.SEVERE, "AZ:    EXCEPTION: ", e);
            permit = false;
        }

        if (! permit) {
            logger.log(Level.WARNING, "AZ:    Access denied. Sorry. ");
            servletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "YOU CANNOT ACCESS THE SENSITIVE DATA");
            return false;
        } else {
            logger.log(Level.INFO, "AZ:    Access granted. ");
            return true;
        }
    }
}