# ABAC Valve

ABAC authorization middleware implemented as a [Tomcat Valve](https://tomcat.apache.org/tomcat-9.0-doc/api/org/apache/catalina/valves/ValveBase.html).
Ported from this [simple client app](https://gitlab.com/asclepios-project/simple-client-app)

## Build

1. Install ABAC authorization libararies

```
git clone https://gitlab.com/asclepios-project/abac-authorization.git
cd abac-authorization
git checkout asclepios/R1
mvn clean install
cd ..
```

2. Build fat jar of ABAC Valve

```
git clone https://gitlab.com/asclepios-project/abac-valve.git
cd abac-valve
mvn clean package
```

## Install on Tomcat

* `target/abac-valve-0.0.1-SNAPSHOT-jar-with-dependencies.jar` should be copied to `$CATALINA_HOME/lib/`
* `truststore-client.p12` should be copied to `$CATALINA_HOME/`
* Add the following line to `$CATALINA_HOME/conf/server.xml`:
```
<Valve className="eu.asclepios.tomcat.ABACValve"/>

```
* Set environment variables to configure
```
AZ_CLIENT_TRUST_STORE_FILE=truststore-client.p12
AZ_CLIENT_TRUST_STORE_TYPE=PKCS12
AZ_CLIENT_TRUST_STORE_PASSWORD=asclepios
AZ_SERVER_ENDPOINTS=https://localhost:7071/checkJsonAccessRequest
AZ_SERVER_ACCESS_KEY=7235687126587231675321756752657236156321765723
AZ_CALL_DISABLED=false
AZ_CALL_LOAD_BALANCE_METHOD=ORDER
AZ_CALL_RETRIES=1

export AZ_CLIENT_TRUST_STORE_FILE AZ_CLIENT_TRUST_STORE_TYPE AZ_CLIENT_TRUST_STORE_PASSWORD AZ_SERVER_ENDPOINTS AZ_SERVER_ACCESS_KEY AZ_CALL_DISABLED AZ_CALL_LOAD_BALANCE_METHOD AZ_CALL_RETRIES
```

## Links

* [What is Tomcat valve?](https://www.oxxus.net/tutorials/tomcat/tomcat-valve)